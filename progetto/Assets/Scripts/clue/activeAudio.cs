using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class activeAudio : MonoBehaviour
{
    public AudioSource aSource;
    public AudioClip aClip;
    public static bool reload = false;
    void Start()

        {
        if (reload == true)
        {
            aSource.Stop();
            Debug.Log("stop");
        }
        else
        {
            aSource.PlayOneShot(aClip);
            Debug.Log("Play");
        }
    }
}
