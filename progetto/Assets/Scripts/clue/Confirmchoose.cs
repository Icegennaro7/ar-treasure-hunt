using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Confirmchoose : MonoBehaviour
{
    public GameObject images;
    public GameObject yes;
    public GameObject no;
    // Start is called before the first frame update
    public void OnConfirm()
    {
        images.SetActive(true);
        yes.SetActive(true);
        no.SetActive(true);
    }


}
