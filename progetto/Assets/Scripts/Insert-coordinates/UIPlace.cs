using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UIPlace : MonoBehaviour
{
    public GameObject imagex;
    public static UIPlace Instance { set; get; }
    public GameObject Place1, Place2, Place3, Place4, Place5, Place6,Place7;
    //public GameObject Place;
    //public GameObject Button;
    public InputField lat1,lat2,lat3,lat4,lat5,lat6,lat7;
    public InputField lon1,lon2,lon3,lon4,lon5,lon6,lon7;
    public double la1, la2, la3, la4, la5, la6,la7;
    public double lo1, lo2, lo3, lo4, lo5, lo6,lo7;
    public int i = 1;
    public DataManager dataPlace;
    // Start is called before the first frame update
    void Start()
    {
        Instance = this;
        dataPlace.Load();
        //lat1.text = dataPlace.place.Lat1;
        //lon1.text = dataPlace.place.Lon1;

    }
    public void ChangeCoodinates(string text)
    {

        if (i == 1)
        {
            dataPlace.place.Lat1 = lat1.text;
            dataPlace.place.Lon1 = lon1.text;
            

        }
        else if (i == 2)
        {
            dataPlace.place.Lat2 = lat2.text;
            dataPlace.place.Lon2 = lon2.text;
            
        }
        else if (i == 3)
        {
            dataPlace.place.Lat3 = lat3.text;
            dataPlace.place.Lon3 = lon3.text;
           
        }
        else if (i == 4)
        {
            dataPlace.place.Lat4 = lat4.text;
            dataPlace.place.Lon4 = lon4.text;
            
        }
        else if (i == 5)
        {
            dataPlace.place.Lat5 = lat5.text;
            dataPlace.place.Lon5 = lon5.text;
            
        }
        else if (i == 6)
        {
            dataPlace.place.Lat6 = lat6.text;
            dataPlace.place.Lon6 = lon6.text;
            
        }
        else if (i == 7)
        {
            dataPlace.place.Lat7 = lat7.text;
            dataPlace.place.Lon7 = lon7.text;

        }

    }

    public void ClickSave()
    {
        dataPlace.Save();
        if (i < 7)
            i++;
        else
        {
            i = 1;
           
        }
       if (i==1){
            Place1.SetActive(true);
            Place2.SetActive(false);
            Place3.SetActive(false);
            Place4.SetActive(false);
            Place5.SetActive(false);
            Place6.SetActive(false);
            Place7.SetActive(false);

        }

        else if (i==2)
        {
            Place1.SetActive(false);
            Place2.SetActive(true);
        }
        else if (i == 3)
        {
            Place2.SetActive(false);
            Place3.SetActive(true);
        }
        else if (i == 4)
        {
            Place3.SetActive(false);
            Place4.SetActive(true);
        }
        else if (i == 5)
        {
            Place4.SetActive(false);
            Place5.SetActive(true);
        }
        else if (i == 6)
        {
            Place5.SetActive(false);
            Place6.SetActive(true);
        }
        else if (i == 7)
        {
            Place1.SetActive(false);
            Place2.SetActive(false);
            Place3.SetActive(false);
            Place4.SetActive(false);
            Place5.SetActive(false);
            Place6.SetActive(false);
            Place7.SetActive(true);
        }
        la1 = double.Parse(dataPlace.place.Lat1);
        lo1 = double.Parse(dataPlace.place.Lon1);
        la2 = double.Parse(dataPlace.place.Lat2);
        lo2 = double.Parse(dataPlace.place.Lon2);
        la3 = double.Parse(dataPlace.place.Lat3);
        lo3 = double.Parse(dataPlace.place.Lon3);
        la4 = double.Parse(dataPlace.place.Lat4);
        lo4 = double.Parse(dataPlace.place.Lon4);
        la5 = double.Parse(dataPlace.place.Lat5);
        lo5 = double.Parse(dataPlace.place.Lon5);
        la6 = double.Parse(dataPlace.place.Lat6);
        lo6 = double.Parse(dataPlace.place.Lon6);
        la7 = double.Parse(dataPlace.place.Lat7);
        lo7 = double.Parse(dataPlace.place.Lon7);

    }
    public void ClickReturns()
    {

        imagex.SetActive(true);
        SceneManager.LoadScene("Menu");

    }




}
