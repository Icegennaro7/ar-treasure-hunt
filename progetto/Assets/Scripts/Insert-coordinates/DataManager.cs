using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.SceneManagement;

public class DataManager : MonoBehaviour
{
    public Place place;
    public GameObject imagez;
    string file = "Savefiles.txt";

    public void Save()
    {
        string json = JsonUtility.ToJson(place);
        WriteToFile(file, json);
    }
    public void Load()
    {
        place = new Place();
        string json = ReadFromFile(file);
        JsonUtility.FromJsonOverwrite(json, place);

    }
    private void WriteToFile(string fileName, string json)
    {
        string path = GetFilePath(fileName);
        FileStream fileStream = new FileStream(path, FileMode.Create);

        using (StreamWriter writer = new StreamWriter(fileStream))
        {
            writer.Write(json);
        }
    }
    private string ReadFromFile(string fileName)
    {
        string path = GetFilePath(fileName);
        if (File.Exists(path))
        {
            using (StreamReader reader = new StreamReader(path))
            {
                string json = reader.ReadToEnd();
                return json;
            }
        }
        else
            Debug.LogWarning("File not found:");

        return "";
    }

    private string GetFilePath(string fileName)
    {
        return Application.persistentDataPath + "/" + fileName;
    }

    private void OnApplicationPause(bool pause)
    {
        if (pause)
            Save();
    }
    public void ClickReturn()
    {
        
         imagez.SetActive(true);
        if(SceneManager.GetActiveScene().name == "OUTDOOR")
            SceneManager.LoadScene("Director");
        else if (SceneManager.GetActiveScene().name == "Insert-coordinates")
            SceneManager.LoadScene("provaread");
        else
            SceneManager.LoadScene("Menu");

    }
}
