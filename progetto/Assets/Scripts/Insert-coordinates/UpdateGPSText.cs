using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class UpdateGPSText : MonoBehaviour
{
    public Text coordinateLAT;
    public Text coordinateLON;
    //public Text place;

    void Update()
    {
        coordinateLAT.text = " LAT:" + GPS.Instance.latitude.ToString();
        coordinateLON.text = " LON:" + GPS.Instance.longitude.ToString();
       // place.text = " Lat2:" + Distance.Instance.latB.ToString() + "  Lon2:" + Distance.Instance.lonB.ToString();
    }
}
 