using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class GPS : MonoBehaviour
{
    public static GPS Instance { set; get; }
    public float latitude;
    public float longitude;
    public float accuracy;
    // Start is called before the first frame update
    void Start()
    {
        Instance = this;
        DontDestroyOnLoad(gameObject);
        StartCoroutine(StartLocationService());
    }

    private IEnumerator StartLocationService()
    {
        if(!Input.location.isEnabledByUser)
        {
            Debug.Log("User has not enable GPS");
            yield break;
        }

        Input.location.Start();
        int maxWait = 20;
        while(Input.location.status == LocationServiceStatus.Initializing && maxWait > 0)
        {
            yield return new WaitForSeconds(1);
            maxWait--;
        }
        if(maxWait<=0)
        {
            Debug.Log("Timed out");
            yield break;
        }
        if(Input.location.status == LocationServiceStatus.Failed)
        {
            Debug.Log("Unable to determin device location");
            yield break;
        }
        latitude = Input.location.lastData.latitude;
        longitude = Input.location.lastData.longitude;

        yield break;
    }
    private void Update()
    {
        Input.location.Start(0.5f,1f);//Starts location service updates 
        //Input.location.Start(float desiredAccuracyInMeters, float updateDistanceInMeters);
        latitude = Input.location.lastData.latitude; //Last measured device latitude.
        longitude = Input.location.lastData.longitude;//Last measured device longitude.
        accuracy = Input.location.lastData.horizontalAccuracy;//Horizontal accuracy of the location.
    }
}
