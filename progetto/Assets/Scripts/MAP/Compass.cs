using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
public class Compass : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {
        Input.compass.enabled = true;
    }
    // Baidu use Input.compass to achieve the compass


    // OnGUI calls
    /*void OnGUI()
    {
        //Input.location.Start();
        ////Geographical North Pole is the North, also the magnetic South Pole
        //GUILayout.Label(" rawVector: " + Input.compass.rawVector.ToString() //Original geomagnetic data measured with microteslas
        // + " trueHeading: " + Input.compass.trueHeading.ToString() // corresponds to the degree of geographic North Pole
        // + " headingAccuracy: " + Input.compass.headingAccuracy.ToString() //The accuracy of the title degree
        // + " magneticHeading: " + Input.compass.magneticHeading.ToString(), GUILayout.Width(5000), GUILayout.Width(200));//degrees relative to the magnetic north pole
    }*/
    
    /*[SerializeField] Text text;
     [SerializeField] Text text1;
     [SerializeField] Text text2;
     [SerializeField] Text text3;*/
    [SerializeField] Image compass;
    float dushu = 0; // record the northern degree
         float tempdushu = 0; // temporarily record data to determine whether the angle change is greater than 2
    // Update is called once per frame
    void FixedUpdate()
    {
        // How to determine the reference
        //When the degree is 358-2 degrees, the front of the phone is the north.


        
        Input.location.Start();

        /* text.text = "rawVector: " + Input.compass.rawVector.ToString();//original geomagnetic data measured with microteslas


                   // Corresponding to the degree of geographical North Pole, the phone head is facing the direction North 360 / 0 East 90 West 180 South 270
          text1.text = " trueHeading: " + Input.compass.trueHeading.ToString();

          text2.text = " headingAccuracy: " +Input.compass.headingAccuracy.ToString(); //The accuracy of the title degree
          text3.text = " magneticHeading: " + Input.compass.magneticHeading.ToString();//// degrees relative to the magnetic north pole
        */
       
        dushu = Input.compass.trueHeading;
     
        /*trueHeading          image/z
             North 358 360 0 2 0
               Oriental 88 92 90
               South 269 272 270
               Western 180 180
        */

        // Assignment to prevent the jitter degree from changing more than two
        if (Mathf.Abs(tempdushu - dushu) > 3)
        {
            tempdushu = dushu ;
            compass.transform.eulerAngles = new Vector3(0, 0, dushu-Distance.Instance.degrees);
        }


    }
}