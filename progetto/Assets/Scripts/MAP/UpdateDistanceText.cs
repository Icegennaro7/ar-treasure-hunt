using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class UpdateDistanceText : MonoBehaviour
{
    public Text distances;
    public Text Haccuracy;
    void Update()
    {
        distances.text = " Meters: " + Distance.Instance.d.ToString();
        Haccuracy.text = " Accuracy: " + GPS.Instance.accuracy.ToString();

    }
}
