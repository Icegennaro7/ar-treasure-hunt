using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class UpGPSText : MonoBehaviour
{
    public Text coor;
    public Text place;
   
    void Update()
    {
        coor.text = " Lat1:" + GPS.Instance.latitude.ToString() + "  Lon1:" + GPS.Instance.longitude.ToString();
        place.text = " Lat2:" + Distance.Instance.latB.ToString() + "  Lon2:" + Distance.Instance.lonB.ToString();
    }
}
 