using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class confirmscan : MonoBehaviour
{
    public GameObject sure;

    // Start is called before the first frame update
    public void OnConfirm()
    {
        sure.SetActive(true);
    }

    public void OnYes()
    {
        if (SceneManager.GetActiveScene().name == "Map1")
            SceneManager.LoadScene("OpenMap2");
        else if (SceneManager.GetActiveScene().name == "Map2")
            SceneManager.LoadScene("OpenMap3");
        else if (SceneManager.GetActiveScene().name == "Map3")
            SceneManager.LoadScene("OpenMap4");
        else if (SceneManager.GetActiveScene().name == "Map4")
            SceneManager.LoadScene("OpenMap5");
        else if (SceneManager.GetActiveScene().name == "Map5")
            SceneManager.LoadScene("OpenMap6");
        else if (SceneManager.GetActiveScene().name == "Map6")
            SceneManager.LoadScene("OpenMap7");
        else if (SceneManager.GetActiveScene().name == "Map7")
            SceneManager.LoadScene("Treasure");
    }

    public void OnNo()
    {
        sure.SetActive(false);
    }

}