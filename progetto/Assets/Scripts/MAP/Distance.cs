using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.SceneManagement;
public class Distance : MonoBehaviour
{
    public static Distance Instance {set; get;}
    double latA, lonA;
    public double latB, lonB;
    public int d;
    public float degrees;
    void Start()
    {
        if (SceneManager.GetActiveScene().name == "Map1")
        {
            latB = UIPlace.Instance.la1;
            lonB = UIPlace.Instance.lo1;
        }
        else if (SceneManager.GetActiveScene().name == "Map2")
        {
            latB = UIPlace.Instance.la2;
            lonB = UIPlace.Instance.lo2;
        }
        else if (SceneManager.GetActiveScene().name == "Map3")
        {
            latB = UIPlace.Instance.la3;
            lonB = UIPlace.Instance.lo3;
        }
        else if (SceneManager.GetActiveScene().name == "Map4")
        {
            latB = UIPlace.Instance.la4;
            lonB = UIPlace.Instance.lo4;
        }
        else if (SceneManager.GetActiveScene().name == "Map5")
        {
            latB = UIPlace.Instance.la5;
            lonB = UIPlace.Instance.lo5;
        }
        else if (SceneManager.GetActiveScene().name == "Map6")
        {
            latB = UIPlace.Instance.la6;
            lonB = UIPlace.Instance.lo6;
        }
        else if (SceneManager.GetActiveScene().name == "Map7")
        {
            latB = UIPlace.Instance.la7;
            lonB = UIPlace.Instance.lo7;
        }

        Instance = this;
        DontDestroyOnLoad(gameObject);
    }
    void Update()
    {
        //const double pigreco = 3.1415927;
        /* Defines constants and variables */
        const double R = 6371;
        latA = GPS.Instance.latitude;
        lonA = GPS.Instance.longitude;
        
        double lat_alfa, lat_beta;
        double lon_alfa, lon_beta;
        double deltalon,deltaphi,theta,deg;
        double p,d1;
        /* Converts degrees to radians */
        lat_alfa = Math.PI * latA / 180; // Player latitude
        lat_beta = Math.PI * latB / 180; // Marker latitude
        lon_alfa = Math.PI * lonA / 180; // Player longitude
        lon_beta = Math.PI * lonB / 180; // Marker longitude
        /* Calcola l'angolo compreso fi */
        deltalon = lon_alfa - lon_beta;
        /* Calculate the third side of the spherical triangle*/
        p = Math.Acos(Math.Sin(lat_beta) * Math.Sin(lat_alfa) +
          Math.Cos(lat_beta) * Math.Cos(lat_alfa) * Math.Cos(deltalon));
        /* Calculate the distance on the surface
        Radius of the earth R = ~6371 km */
        d1 = p * R * 1000; // distance in meters
        d =(int) d1;// to get integer
        deltaphi = Math.Log(Math.Tan(lat_beta / 2 + Math.PI / 4) /Math.Tan(lat_alfa / 2 + Math.PI / 4));
        theta = Math.Atan2(deltalon,deltaphi);
        deg = theta * 180 / Math.PI;
        if (deg < 0)
        {
            deg = -deg;
        }
        else
        {
            deg = 360 - deg;
        }
        degrees = (float) deg;
        

    }
}
