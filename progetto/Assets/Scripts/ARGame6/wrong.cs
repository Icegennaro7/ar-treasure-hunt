using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class wrong : MonoBehaviour
{
    public AudioSource aSource;
    public AudioClip aClip;

    public void OnWrongClick()
    {
        aSource.PlayOneShot(aClip);
    }

}
