using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class button23 : MonoBehaviour
{
    public AudioSource aSource;
    public AudioClip aClip;
    public AudioClip aClipX;
    public static bool b23 = false;
    public GameObject ritenta;
    public GameObject primo;
    
    public void ClickB23()
    {

        if (b23 == false && button13.b13 == true && button13.numclick1 <= 2)
        {
            aSource.PlayOneShot(aClip);
            primo.SetActive(false);
            ritenta.SetActive(false);
            button13.numclick1 += 1;
            b23 = true;
            Debug.Log("b23=true");

        }
        else if (button13.numclick1 == 2)
        {
            aSource.PlayOneShot(aClipX);
            button13.b13 = false;
            b23 = false;
            ritenta.SetActive(true);
            Debug.Log("Ritenta");
            button13.numclick1 = 0;
            if (button13.ind13 == true)
            {
                primo.SetActive(true);
                Debug.Log("Indovinato il primo");
                button13.ind13 = false;
               
            }
           
        }
        else
        {
            aSource.PlayOneShot(aClip);
            primo.SetActive(false);
            ritenta.SetActive(false);
            button13.numclick1 += 1;
            b23= false;
            button13.b13 = false;
           Debug.Log("b23=false");
        }
    }
}
