using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class button13 : MonoBehaviour
{
    public AudioSource aSource;
    public AudioClip aClip;
    public AudioClip aClipX;
    public static bool b13 = false;
    public static int numclick1 = 0;
    public GameObject ritenta;
    public static bool ind13 = false;
   public GameObject primo;
    
    public void ClickB13()
    {

        if (button23.b23 == false && b13 == false && numclick1 == 0)
        {
            aSource.PlayOneShot(aClip);
            primo.SetActive(false);
            ind13 = true;
           ritenta.SetActive(false);
            numclick1 += 1;
            b13 = true;
            Debug.Log("b13=true");
        }
        else if (numclick1 == 2)
        {
            aSource.PlayOneShot(aClipX);
            b13 = false;
            button23.b23 = false;
            ritenta.SetActive(true);
            Debug.Log("Ritenta");
            numclick1 = 0;
            if (ind13 == true)
            {
                Debug.Log("Indovinato il primo");
                primo.SetActive(true);
                ind13 = false;
               
            }
        
        }
        else
        {
            aSource.PlayOneShot(aClip);
            primo.SetActive(false);
            ritenta.SetActive(false);
            numclick1 += 1;
            b13 = false;
            button23.b23 = false;
            Debug.Log("b13=false");

        }

    }

}
