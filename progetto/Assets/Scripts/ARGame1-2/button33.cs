using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class button33 : MonoBehaviour
{
    public AudioSource aSource;
    public AudioClip aClip;
    public AudioClip aClipX;
    public GameObject ritenta;
    public GameObject primo;
    public void ClickB33()
    {

        if (button23.b23 == true && button13.b13 == true && button13.numclick1 <= 2)
        {
          primo.SetActive(false);
          ritenta.SetActive(false);
          button13.numclick1 = 0;
          button13.b13 = false;
          button23.b23 = false;
           button13.ind13 = false;
            Debug.Log("b33=true-victory");
          
            if (SceneManager.GetActiveScene().name == "ARGame1")
            
                SceneManager.LoadScene("clue1");
            else
                SceneManager.LoadScene("clue2");

        }
        else if (button13.numclick1 == 2)
        {
            aSource.PlayOneShot(aClipX);
            Debug.Log("Ritenta");
           ritenta.SetActive(true);
            button13.numclick1 = 0;
            if (button13.ind13 == true)
            {
                primo.SetActive(true);
                button13.ind13 = false;
               
                Debug.Log("Indovinato il primo");
            }
           
        }
        else
        {
            aSource.PlayOneShot(aClip);
            primo.SetActive(false);
            ritenta.SetActive(false);
            button13.numclick1 += 1;
            button13.b13 = false;
            button23.b23 = false;
            Debug.Log("b33=false");
        }
    }
}
