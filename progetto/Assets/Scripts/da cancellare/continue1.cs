using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class continue1 : MonoBehaviour
{
    // Start is called before the first frame update
    public void OnContinue1()
    {
        if (SceneManager.GetActiveScene().name == "choose")
            SceneManager.LoadScene("Rules");
        else if(SceneManager.GetActiveScene().name == "Rules")
        SceneManager.LoadScene("ARGame1");
    }
}
