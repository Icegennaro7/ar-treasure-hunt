using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Play : MonoBehaviour
{
    public string link;
    public void OnPlay()
    {
        if (SceneManager.GetActiveScene().name == "Menu")
            SceneManager.LoadScene("choose");
        else if (SceneManager.GetActiveScene().name == "choose")
            SceneManager.LoadScene("Rules");
        else if (SceneManager.GetActiveScene().name == "Rules")
            SceneManager.LoadScene("ARGame1");
        else if (SceneManager.GetActiveScene().name == "Director")
            SceneManager.LoadScene("OUTDOOR");
        else if (SceneManager.GetActiveScene().name == "OUTDOOR")
            SceneManager.LoadScene("Insert-coordinates");
        else if(SceneManager.GetActiveScene().name == "Treasure")
            SceneManager.LoadScene("Menu");

    }
    public void OnContOut()
    {
        if (SceneManager.GetActiveScene().name == "choose")
            SceneManager.LoadScene("RulesOUTDOOR");
        else if (SceneManager.GetActiveScene().name == "RulesOUTDOOR")
            SceneManager.LoadScene("OpenMap1");
        else if (SceneManager.GetActiveScene().name == "OpenMap1")
            SceneManager.LoadScene("Map1");
        else if (SceneManager.GetActiveScene().name == "OpenMap2")
            SceneManager.LoadScene("Map2");
        else if (SceneManager.GetActiveScene().name == "OpenMap3")
            SceneManager.LoadScene("Map3");
        else if (SceneManager.GetActiveScene().name == "OpenMap4")
            SceneManager.LoadScene("Map4");
        else if (SceneManager.GetActiveScene().name == "OpenMap5")
            SceneManager.LoadScene("Map5");
        else if (SceneManager.GetActiveScene().name == "OpenMap6")
            SceneManager.LoadScene("Map6");
        else if (SceneManager.GetActiveScene().name == "OpenMap7")
            SceneManager.LoadScene("Map7");
    }
    public void Onreturn()
    {
        if (SceneManager.GetActiveScene().name == "OpenMap1")
            SceneManager.LoadScene("choose");
        else if (SceneManager.GetActiveScene().name == "OpenMap2")
            SceneManager.LoadScene("Map1");
        else if (SceneManager.GetActiveScene().name == "OpenMap3")
            SceneManager.LoadScene("Map2");
        else if (SceneManager.GetActiveScene().name == "OpenMap4")
            SceneManager.LoadScene("Map3");
        else if (SceneManager.GetActiveScene().name == "OpenMap5")
            SceneManager.LoadScene("Map4");
        else if (SceneManager.GetActiveScene().name == "OpenMap6")
            SceneManager.LoadScene("Map5");
        else if (SceneManager.GetActiveScene().name == "OpenMap7")
            SceneManager.LoadScene("Map6");

    }
    public void OpenDirector()
    {
        SceneManager.LoadScene("Director");
    }
    
    public void OpenUrlOnBrowser()
    {
        Application.OpenURL(@link);
    }

    public void OnExit()
    {
        Application.Quit();
    }
    public void ReturnHome()
    {
        SceneManager.LoadScene("Menu");
    }
}
