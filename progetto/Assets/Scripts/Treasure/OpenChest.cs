using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenChest : MonoBehaviour
{
    public GameObject open;
    public GameObject close;
    public GameObject fireworks;
    public GameObject text;
    public GameObject victory;
    public GameObject aSource;
    public GameObject button;
    void OnMouseDown()
        {
            close.SetActive(false);
            open.SetActive(true);
            fireworks.SetActive(true);
            text.SetActive(false);
            victory.SetActive(true);
            aSource.SetActive(true);
            button.SetActive(true);
        }

    // if (transform.rotation.z != Random.Range(-150f, -30f))
}
