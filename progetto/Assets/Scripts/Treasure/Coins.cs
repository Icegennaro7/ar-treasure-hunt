using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;
using static Vuforia.TrackableBehaviour;

public class Coins : MonoBehaviour
{
    private TrackableBehaviour mTrackableBehaviour;
    
    
    public GameObject firecoins;
    public GameObject aSource;


    // Start is called before the first frame update
    void Start()
    {
        mTrackableBehaviour = GetComponent<TrackableBehaviour>();

        if (mTrackableBehaviour)
        {

            mTrackableBehaviour.RegisterOnTrackableStatusChanged(OnTrackableStatusChanged);

        }
    }
    protected virtual void OnTrackingFound()
    {
        var rendererComponents = GetComponentsInChildren<Renderer>(true);
        var colliderComponents = GetComponentsInChildren<Collider>(true);
        var canvasComponents = GetComponentsInChildren<Canvas>(true);

        // Enable rendering:
        foreach (var component in rendererComponents)
            component.enabled = true;

        // Enable colliders:
        foreach (var component in colliderComponents)
            component.enabled = true;

        // Enable canvas':
        foreach (var component in canvasComponents)
            component.enabled = true;
    }


    protected virtual void OnTrackingLost()
    {
        var rendererComponents = GetComponentsInChildren<Renderer>(true);
        var colliderComponents = GetComponentsInChildren<Collider>(true);
        var canvasComponents = GetComponentsInChildren<Canvas>(true);

        
        // Disable rendering:
        foreach (var component in rendererComponents)
            component.enabled = false;

        // Disable colliders:
        foreach (var component in colliderComponents)
            component.enabled = false;

        // Disable canvas':
        foreach (var component in canvasComponents)
            component.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {

    }
    public void OnTrackableStatusChanged(StatusChangeResult obj)
    {
        if (obj.NewStatus == Status.DETECTED ||
            obj.NewStatus == Status.TRACKED  /*  ||
          obj.NewStatus == Status.EXTENDED_TRACKED*/)
        {

            Debug.Log("Trackable " + mTrackableBehaviour.TrackableName + " found");

          
            OnTrackingFound();
        }
        else if (obj.PreviousStatus == TrackableBehaviour.Status.TRACKED ||
          obj.NewStatus == Status.EXTENDED_TRACKED &&
                  obj.NewStatus == TrackableBehaviour.Status.NO_POSE)
        {
            Debug.Log("Trackable " + mTrackableBehaviour.TrackableName + " lost");
          
            firecoins.SetActive(false);
            aSource.SetActive(false);
            OnTrackingLost();
        }
        else
        {
            // For combo of previousStatus=UNKNOWN + newStatus=UNKNOWN|NOT_FOUND
            // Vuforia is starting, but tracking has not been lost or found yet
            // Call OnTrackingLost() to hide the augmentations
            
            OnTrackingLost();
        }
    }
}
