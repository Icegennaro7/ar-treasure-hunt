using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class button4 : MonoBehaviour
{
    public AudioSource aSource;
    public AudioClip aClip;
    public AudioClip aClipX;
    public GameObject ritenta;
    public GameObject primo;
    public GameObject primidue;

    public void ClickB4()
    {

        if (button3.b3 == true && button2.b2 == true && button1.b1 == true && button1.numclick <= 3)
        {
            primo.SetActive(false);
            primidue.SetActive(false);
            ritenta.SetActive(false);
            button1.numclick = 0;
            button1.b1 = false;
            button2.b2 = false;
            button3.b3 = false;
            button1.ind1 = false;
            button2.ind2 = false;
            Debug.Log("b4=true-victory");
            if (SceneManager.GetActiveScene().name == "ARGame3")

                SceneManager.LoadScene("clue3");

            else if(SceneManager.GetActiveScene().name == "ARGame4")
                SceneManager.LoadScene("clue4");
            else if (SceneManager.GetActiveScene().name == "ARGame5")
                SceneManager.LoadScene("clue5");

        }
        else if (button1.numclick == 3)
        {
            button1.b1 = false;
            button2.b2 = false;
            button3.b3 = false;
            aSource.PlayOneShot(aClipX);
            Debug.Log("Ritenta");
            ritenta.SetActive(true);
            button1.numclick = 0;
            if (button2.ind2 == false && button1.ind1 == true)
            {
                primo.SetActive(true);
                button1.ind1 = false;
                button2.ind2 = false;
                Debug.Log("Indovinato il primo");
            }
            else if (button2.ind2 == true)
            {
                primidue.SetActive(true);
                button1.ind1 = false;
                button2.ind2 = false;
                Debug.Log("Indovinati i primi due");
            }

        }
        else
        {
            aSource.PlayOneShot(aClip);
            primo.SetActive(false);
            primidue.SetActive(false);
            ritenta.SetActive(false);
            button1.numclick += 1;
            button1.b1 = false;
            button2.b2 = false;
            button3.b3 = false;
            Debug.Log("b4=false");
        }
    }
}
