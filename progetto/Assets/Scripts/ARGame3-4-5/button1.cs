using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class button1 : MonoBehaviour
{
    public AudioSource aSource;
    public AudioClip aClip;
    public AudioClip aClipX;
    public static bool b1 = false;
    public static int numclick = 0;
    public GameObject ritenta;
    public static bool ind1 = false;
    public GameObject primo;
    public GameObject primidue;
    public void ClickB1()
    {

        if (button3.b3 == false && button2.b2 == false && b1 == false && numclick == 0)
        {
            aSource.PlayOneShot(aClip);
            primo.SetActive(false);
            primidue.SetActive(false);
            ind1 = true;
            ritenta.SetActive(false);
            numclick += 1;
            b1 = true;
            Debug.Log("b1=true");
        }
        else if (numclick == 3)
        {
            b1 = false;
            button2.b2 = false;
            button3.b3 = false;
            aSource.PlayOneShot(aClipX);
            ritenta.SetActive(true);
            Debug.Log("Ritenta");
            numclick = 0;
            if (button2.ind2 == false && ind1 == true)
            {
                Debug.Log("Indovinato il primo");
                primo.SetActive(true);
                ind1 = false;
                button2.ind2 = false;
            }
            else if (button2.ind2 == true)
            {
                primidue.SetActive(true);
                ind1 = false;
                button2.ind2 = false;
                Debug.Log("Indovinati i primi due");
            }
        }
        else
        {
            aSource.PlayOneShot(aClip);
            primidue.SetActive(false);
            primo.SetActive(false);
            ritenta.SetActive(false);
            numclick += 1;
            b1 = false;
            button2.b2 = false;
            button3.b3 = false;

            Debug.Log("b1=false");

        }

    }
}
