using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class button2 : MonoBehaviour
{
    public AudioSource aSource;
    public AudioClip aClip;
    public AudioClip aClipX;
    public static bool b2 = false;
    public GameObject ritenta;
    public static bool ind2 = false;
    public GameObject primo;
    public GameObject primidue;
    public void ClickB2()
    {

        if (button3.b3 == false && b2 == false && button1.b1 == true && button1.numclick <= 3)
        {
            aSource.PlayOneShot(aClip);
            primo.SetActive(false);
            primidue.SetActive(false);
            ind2 = true;
            ritenta.SetActive(false);
            button1.numclick += 1;
            b2 = true;
            Debug.Log("b2=true");

        }
        else if (button1.numclick == 3)
        {
            b2 = false;
            button1.b1 = false;
            button3.b3 = false;
            aSource.PlayOneShot(aClipX);
            ritenta.SetActive(true);
            Debug.Log("Ritenta");
            button1.numclick = 0;
            if (ind2 == false && button1.ind1 == true)
            {
                primo.SetActive(true);
                Debug.Log("Indovinato il primo");
                button1.ind1 = false;
                ind2 = false;
            }
            else if (ind2 == true)
            {
                primidue.SetActive(true);
                button1.ind1 = false;
                ind2 = false;
                Debug.Log("Indovinati i primi due");
            }
        }
        else
        {
            aSource.PlayOneShot(aClip);
            primo.SetActive(false);
            primidue.SetActive(false);
            ritenta.SetActive(false);
            button1.numclick += 1;
            b2 = false;
            button1.b1 = false;
            button3.b3 = false;
            Debug.Log("b2=false");
        }
    }
}
