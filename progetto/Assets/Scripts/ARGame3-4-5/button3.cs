using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class button3 : MonoBehaviour
{
    public AudioSource aSource;
    public AudioClip aClip;
    public AudioClip aClipX;
    public static bool b3 = false;
    public GameObject ritenta;
    public GameObject primo;
    public GameObject primidue;

    public void ClickB3()
    {

        if (b3 == false && button2.b2 == true && button1.b1 == true && button1.numclick <= 3)
        {
            aSource.PlayOneShot(aClip);
            primo.SetActive(false);
            primidue.SetActive(false);
            ritenta.SetActive(false);
            button1.numclick += 1;
            b3 = true;
            Debug.Log("b3=true");

        }
        else if (button1.numclick == 3)
        {
            b3 = false;
            button1.b1 = false;
            button2.b2 = false;
            aSource.PlayOneShot(aClipX);
            ritenta.SetActive(true);
            Debug.Log("Ritenta");
            button1.numclick = 0;
            if (button2.ind2 == false && button1.ind1 == true)
            {
                primo.SetActive(true);
                Debug.Log("Indovinato il primo");
                button1.ind1 = false;
                button2.ind2 = false;
            }
            else if (button2.ind2 == true)
            {
                primidue.SetActive(true);
                button1.ind1 = false;
                button2.ind2 = false;
                Debug.Log("Indovinati i primi due");
            }
        }
        else
        {
            aSource.PlayOneShot(aClip);
            primo.SetActive(false);
            primidue.SetActive(false);
            ritenta.SetActive(false);
            button1.numclick += 1;
            b3 = false;
            button1.b1 = false;
            button2.b2 = false;
            Debug.Log("b3=false");
        }
    }
}
