HOME
Places to hide the markers:
the first marker(NanatsunoTaizai) must be shown to the player.
marker 2(Naruto)-> aroud stuffed animal
marker 3(My ero academia)-> aroud kitchen sink
marker 4(demon slayer)-> in the microwave oven
marker 5(Aot)-> around the couch
marker 6(Onepiece)-> in the fridge
marker 7(chest)-> around a computer
Make sure the player follows in the right path, the right order of markers.

OUTDOOR
Places to hide the markers:
the first marker(Yamcha_Logo) must be shown to the player.
Place the other markers according to the GPS coordinates, which you entered by clicking on the 'Enter GPS coordinates' button.
Here too the order is important, the right order to follow is indicated in the name of the marker.
So 1-Yamcha_Logo will be the first,
2-rekaio will be the second,
3-Kanji_Piccolo_kami will be the third, and so on.
Once the markers have been placed, enter the coordinates on the player's cell phone.
Make sure the player follows in the right path, the right order of markers.
